<?php //Bismillah
/*
Test Game:
http://www.counttonine.com/sudokuGame.htm
Game: 69859
*/

?>
<head>
<title>Sudoku Solver</title>
<style type="text/css">
.btop
{
	border-top: 1px solid #333;
}
.bbottom
{
	border-bottom: 1px solid #333;
}
.bleft
{
	border-left: 1px solid #333;
}
.bright
{
	border-right: 1px solid #333;
}
td
{
	padding: 5px;
}

#dvtable
{
	float: left;
}
#txloader
{
	width: 350px;
	height: 350px;
}

.txs
{
	width: 25px;
}

.fixed
{
	background-color: #AAA;
}
.solved
{
	background-color: #00AA00;
}
.test
{
	background-color: #FF0000;
}
</style>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
var grid = {};
var solved = 0;
var iterations = 0;
var lims = new Array();

function init()
{
	for (var y=0; y<9; y++)
	{
		for (var x=0; x<9; x++)
		{
			id = x+"_"+y;
			bid = "b"+id;
			grid[bid] = {
				'fixed': false,
				'value': false,
				'poss': "123456789"
				};
			if ($('#tx'+id).val()!="")
			{
				$('#td'+id).addClass('fixed');
				grid[bid].fixed = $('#tx'+id).val();
				grid[bid].value = grid[bid].fixed;
				grid[bid].poss = grid[bid].fixed;
			}
		}
	}
	console.log(grid);
}

function save()
{
	var sgrid = {};
	for (var y=0; y<9; y++)
	{
		for (var x=0; x<9; x++)
		{
			id = x+"_"+y;
			sgrid["b"+id] = $('#tx'+id).val();
		}
	}
	$('#txloader').val( JSON.stringify(sgrid) );
}
function load()
{
	grid = JSON.parse( $('#txloader').val() );
	for (var y=0; y<9; y++)
	{
		for (var x=0; x<9; x++)
		{
			id = x+"_"+y;
			if (grid["b"+id]!="")
			{
				$('#tx'+id).val(grid["b"+id]);
			}
		}
	}
}

function box(x,y)
{
	return grid["b"+x+"_"+y];
}

function solve()
{
	//while (!checkComplete())
	//{
		solved = 0;
		for (var y=0; y<9; y++)
		{
			for (var x=0; x<9; x++)
			{
				iterate(x,y);
			}
		}
		iterations++;
		console.log("Iterations: "+iterations+", solved: "+solved); //DEBUG
	
		if (solved==0)
		{
			limCheck();
		}
	//}
	
	
	
	if (checkComplete())
	{
		console.log("Solved!");
	}
}
function iterate(x,y)
{
	var b = box(x,y);
	if (!b.value)
	{
		//console.log('Checking '+x+","+y+".."); //DEBUG
		//Check Horizontally and Vertically
		for (var i = 0; i<9; i++)
		{
			if (i!=x)
			{
				testPoss(b,i,y);
			}
			if (i!=y)
			{
				testPoss(b,x,i);
			}
		}
		
		//Check Square
		var xL = x-(x%3);
		var yL = y-(y%3);
		for (var k=0; k<3; k++)
		{
			for (var j=0; j<3; j++)
			{
				testPoss(b, xL+j, yL+k);
			}
		}	
		
		//Check if solution determinable
		if (b.poss.length==1)
		{
			markAnswer(x,y, b.poss);
		}
	}
}

function limCheck()
{
	console.log("Beginning Limitation Check.."); //DEBUG
	for (var y=0; y<9; y++)
	{
		for (var x=0; x<9; x++)
		{	
			var b = box(x,y);
			if (!b.value)
			{
				//Conditions: Row, Column, Square
				var funcs = [
					function(xx,yy,ii,bb) { if (ii!=xx) { testLims(bb.poss,ii,yy); } },
					function(xx,yy,ii,bb) { if (ii!=yy) { testLims(bb.poss,xx,ii);} },
					function(xx,yy,ii,bb) {
							var xQ = xx-(xx%3)+(ii%3);
							var  yQ = yy-(yy%3)+Math.floor(ii/3);
							//console.log("Pair: "+xx+','+yy+" - "+xQ+','+yQ);
							if (!(xx==xQ&&yy==yQ))
							{
								//console.log('testing '+xQ+','+yQ); //DEBUG
								testLims(bb.poss, xQ, yQ);
								//$('#td'+xQ+"_"+yQ).addClass('test'); //DEBUG
							}
						}
					];
				
				var fun = ['row','column','square'];//DEBUG
				
				for (var h=0; h<3; h++)
				{
					console.log('Testing '+fun[h]); //DEBUG
					lims = new Array();
					for (var p=0; p<b.poss.length; p++)
					{
						lims[b.poss.substr(p,1)]= 0;
					}
					for (var i=0; i<9; i++)
					{
						//console.log('i:'+i);//DEBUG
						funcs[h](x,y,i,b);
					}
					//Check if any of the limits remain at 0;
					console.log(lims); //DEBUG
					for (var j=0; j<b.poss.length; j++)
					{
						if (lims[b.poss.substr(j,1)]==0)
						{
							markAnswer(x,y, b.poss.substr(j,1));
							console.log('answer marked at '+x+','+y);
							break;
						}
					}
				}
			}
			if (solved>0) { break; }
		}
		if (solved>0) { break; }
	}
	console.log("Limitation Check Completed, solved: "+solved); //DEBUG
}

function testLims(poss, x, y)
{
	var b = box(x,y);
	for (var i=0; i<poss.length; i++)
	{
		if (b.poss.indexOf(poss[i])!=-1)
		{
			lims[poss[i]]++;
		}
	}
	//$('#td'+x+"_"+y).addClass('solved');
}

function testPoss(b,x,y)
{
	var c = box(x,y);
	if (c.value)
	{
		b.poss = b.poss.replace(c.value, '');
	}
}

function markAnswer(x,y, val)
{
	var b = box(x,y);
	var id = x+"_"+y;
	b.value = val.toString();
	b.poss = val.toString();
	$('#tx'+id).val(b.value);
	$('#td'+id).addClass('solved');
	solved++;
}

function checkComplete()
{
	for (var y=0; y<9; y++)
	{
		for (var x=0; x<9; x++)
		{
			var b = box(x,y);
			if (!b.value)
			{
				return false;
			}
		}
	}
	return true;
}
</script>
</head>
<body>
<div id="dvtable">
<?php
//Print out sudoku grid
echo "<table>\n";
for ($y=0; $y<9; $y++)
{
	echo "\t<tr>\n";
	for ($x=0; $x<9; $x++)
	{
		$id = $x."_".$y;
		$classes = "tds";
		if ($y%3==0)
		{
			$classes .= " btop";
		}
		if ($y==8)
		{
			$classes .= " bbottom";
		}
		if ($x%3==0)
		{
			$classes .= " bleft";
		}
		if ($x==8)
		{
			$classes .= " bright";
		}
		echo "\t\t<td id='td".$id."' class='".$classes."'>";
		echo "<input type='text' class='txs' id='tx".$id."'></td>\n";
	}
	echo "\t</tr>\n";
}
echo "</table>\n";
?>
</div>
<div style="float: right;">
	<textarea id="txloader">{"b0_0":"1","b1_0":"","b2_0":"","b3_0":"","b4_0":"","b5_0":"2","b6_0":"3","b7_0":"","b8_0":"7","b0_1":"","b1_1":"6","b2_1":"","b3_1":"","b4_1":"","b5_1":"","b6_1":"2","b7_1":"","b8_1":"1","b0_2":"9","b1_2":"","b2_2":"3","b3_2":"","b4_2":"","b5_2":"4","b6_2":"","b7_2":"","b8_2":"","b0_3":"4","b1_3":"","b2_3":"1","b3_3":"","b4_3":"","b5_3":"","b6_3":"7","b7_3":"","b8_3":"5","b0_4":"","b1_4":"","b2_4":"","b3_4":"","b4_4":"","b5_4":"","b6_4":"","b7_4":"","b8_4":"","b0_5":"","b1_5":"","b2_5":"","b3_5":"","b4_5":"9","b5_5":"1","b6_5":"","b7_5":"6","b8_5":"8","b0_6":"","b1_6":"","b2_6":"","b3_6":"","b4_6":"","b5_6":"6","b6_6":"","b7_6":"7","b8_6":"4","b0_7":"2","b1_7":"5","b2_7":"8","b3_7":"","b4_7":"4","b5_7":"","b6_7":"","b7_7":"","b8_7":"","b0_8":"","b1_8":"","b2_8":"","b3_8":"","b4_8":"5","b5_8":"","b6_8":"","b7_8":"","b8_8":""}</textarea>
</div>
<div>
	<button onclick="init();">Initialize</button>
	<button onclick="save();">Save</button>
	<button onclick="load();">Load</button>
	<button onclick="solve();">Solve</button>
</div>
</body>